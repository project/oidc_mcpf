<?php

namespace Drupal\oidc_mcpf\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to edit the module configuration.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The user roles storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userRoleStorage;

  /**
   * The condition manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $conditionManager;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The condition plugin instances.
   *
   * @var \Drupal\Core\Condition\ConditionInterface[]
   */
  protected $conditionPlugins = [];

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $condition_manager
   *   The condition manager.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library discovery service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, PluginManagerInterface $condition_manager, LibraryDiscoveryInterface $library_discovery) {
    parent::__construct($config_factory);

    $this->userRoleStorage = $entity_type_manager->getStorage('user_role');
    $this->conditionManager = $condition_manager;
    $this->libraryDiscovery = $library_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.condition'),
      $container->get('library.discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'oidc_mcpf.settings',
      'oidc_mcpf.idm',
      'oidc_mcpf.toolbar',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oidc_mcpf_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['settings'] += $this->buildSettingsForm($form_state);

    $form['idm'] = [
      '#type' => 'details',
      '#title' => $this->t('IDM'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['idm'] += $this->buildIdmForm($form_state);

    $form['toolbar'] = [
      '#type' => 'details',
      '#title' => $this->t('Toolbar'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['toolbar'] += $this->buildToolbarForm($form, $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Build the settings element form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   *
   * @return array
   *   The form structure.
   */
  protected function buildSettingsForm(FormStateInterface $form_state) {
    $config = $this->config('oidc_mcpf.settings');

    $element = [];

    $element['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#options' => [
        'test' => $this->t('Test'),
        'production' => $this->t('Production'),
      ],
      '#default_value' => $config->get('environment'),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * Build the IDM element form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   *
   * @return array
   *   The form structure.
   */
  protected function buildIdmForm(FormStateInterface $form_state) {
    $config = $this->config('oidc_mcpf.idm');
    $roles = $this->userRoleStorage->loadMultiple();

    // Remove the locked roles.
    unset($roles[UserInterface::ANONYMOUS_ROLE], $roles[UserInterface::AUTHENTICATED_ROLE]);

    // Turn into an [ID => label] array.
    array_walk($roles, static function (&$role) {
      $role = $role->label();
    });

    // Store the roles mapping.
    if (!$form_state->has('idm_roles_mapping')) {
      $roles_mapping = $config->get('roles_mapping') ?: [];

      if ($roles_mapping) {
        $roles_mapping = array_intersect_key($roles_mapping, $roles);
      }

      $form_state->set('idm_roles_mapping', $roles_mapping);
    }

    $element = [];

    $element['roles_claim'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Roles claim'),
      '#description' => $this->t('Don\'t forget to add the matching scope to the <a href=":url">ACM configuration</a>.', [
        ':url' => Url::fromRoute('oidc.admin.realms_config')->toString(TRUE)->getGeneratedUrl(),
      ]),
      '#default_value' => $config->get('roles_claim'),
    ];

    $element['roles_mapping'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles mapping'),
      '#description' => $this->t('Specify the IDM roles in the same format as configured in ACM: @formats. In case ACM provides the roles with a scope, the format with role and context is also supported.', [
        '@formats' => _oidc_mcpf_format_list([
          '<role>-<context>:<scope>',
          '<role>:<context>',
          '<role>',
        ]),
      ]),
      '#open' => TRUE,
      '#parents' => ['idm', '_roles_mapping'],
      '#prefix' => '<div id="oidc-mcpf-idm-roles-mapping">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ':text[name="idm[roles_claim]"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $element['roles_mapping']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Drupal role'),
        $this->t('IDM role'),
        $this->t('Delete'),
      ],
      '#empty' => empty($roles) ? $this->t('There are no Drupal roles that can be mapped.') : $this->t('No roles mapping has been specified.'),
    ];

    foreach ($form_state->get('idm_roles_mapping') as $rid => $idm_role) {
      $row = [];

      $row['drupal_role'] = [
        '#plain_text' => $roles[$rid],
      ];

      $row['idm_role'] = [
        '#type' => 'textfield',
        '#title' => $this->t('IDM role'),
        '#title_display' => 'invisible',
        '#default_value' => $idm_role,
        '#parents' => ['idm', 'roles_mapping', $rid],
      ];

      $row['delete'] = [
        '#type' => 'submit',
        '#name' => 'delete_idm_' . $rid,
        '#value' => $this->t('Delete'),
        '#submit' => ['::deleteIdmRolesMappingEntrySubmit'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'wrapper' => 'oidc-mcpf-idm-roles-mapping',
          'callback' => '::idmRolesMappingAjax',
        ],
        '#rid' => $rid,
      ];

      $element['roles_mapping']['table'][$rid] = $row;

      unset($roles[$rid]);
    }

    if ($roles) {
      asort($roles);
    }

    $element['roles_mapping']['add'] = [
      '#type' => 'details',
      '#title' => $this->t('Add role'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#access' => !empty($roles),
    ];

    $element['roles_mapping']['add']['rid'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal role'),
      '#title_display' => 'invisible',
      '#options' => $roles,
      '#empty_option' => '- ' . $this->t('Select') . ' -',
    ];

    $element['roles_mapping']['add']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#validate' => ['::addIdmRolesMappingEntryValidate'],
      '#submit' => ['::addIdmRolesMappingEntrySubmit'],
      '#limit_validation_errors' => [
        ['idm', '_roles_mapping', 'add'],
      ],
      '#ajax' => [
        'wrapper' => 'oidc-mcpf-idm-roles-mapping',
        'callback' => '::idmRolesMappingAjax',
      ],
    ];

    return $element;
  }

  /**
   * Build the toolbar element form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   *
   * @return array
   *   The form structure.
   */
  protected function buildToolbarForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('oidc_mcpf.toolbar');

    // Store the visibility settings.
    if (!$form_state->has('toolbar_visibility')) {
      $visibility = $config->get('visibility') ?: [];

      if ($visibility) {
        $visibility = array_intersect_key($visibility, $this->conditionManager->getDefinitions());
      }

      $form_state->set('toolbar_visibility', $visibility);
    }

    $element = [];

    $element['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => (int) $config->get('enabled'),
    ];

    $element['toolbar'] = [
      '#type' => 'container',
      '#parents' => ['toolbar'],
      '#states' => [
        'visible' => [
          ':checkbox[name="toolbar[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['toolbar']['widget_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget ID'),
      '#default_value' => $config->get('widget_id'),
      '#states' => [
        'required' => [
          ':checkbox[name="toolbar[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['toolbar']['visibility'] = [
      '#type' => 'details',
      '#title' => $this->t('Visibility'),
      '#open' => TRUE,
      '#parents' => ['toolbar', '_visibility'],
      '#prefix' => '<div id="oidc-mcpf-toolbar-visibility">',
      '#suffix' => '</div>',
    ];

    $element['toolbar']['visibility']['tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    $element['toolbar']['visibility']['conditions'] = [];

    $conditions = [];
    foreach ($this->conditionManager->getDefinitions() as $plugin_id => $definition) {
      // Add as option if it hasn't been enabled.
      if (!$form_state->has(['toolbar_visibility', $plugin_id])) {
        $conditions[$plugin_id] = (string) $definition['label'];
        continue;
      }

      // Get and configure the plugin.
      $condition = $this->getConditionPlugin($plugin_id);

      if ($configuration = $form_state->get(['toolbar_visibility', $plugin_id])) {
        $condition->setConfiguration($configuration);
      }

      // Create the form and form state.
      $condition_form = [
        '#parents' => ['toolbar', 'visibility', $plugin_id],
      ];

      $condition_form_state = SubformState::createForSubform($condition_form, $form, $form_state);
      $condition_form = $condition->buildConfigurationForm($condition_form, $condition_form_state);

      // Wrap it in a details element so it will be shown as vertical tab
      // and add a delete button to remove the condition.
      $condition_form_wrapper = [
        '#type' => 'details',
        '#title' => $condition->getPluginDefinition()['label'],
        '#group' => 'toolbar][_visibility][tabs',
      ];

      $condition_form_wrapper['subform'] = $condition_form;

      $condition_form_wrapper['actions'] = [
        '#type' => 'actions',
      ];

      $condition_form_wrapper['actions']['condition'] = [
        '#type' => 'submit',
        '#name' => 'delete_toolbar_visibility_condition_' . $plugin_id,
        '#value' => $this->t('Delete'),
        '#submit' => ['::deleteToolbarVisibilityConditionSubmit'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'wrapper' => 'oidc-mcpf-toolbar-visibility',
          'callback' => '::toolbarVisibilityAjax',
        ],
        '#plugin_id' => $plugin_id,
      ];

      $element['toolbar']['visibility']['conditions'][$plugin_id] = $condition_form_wrapper;
    }

    if ($conditions) {
      asort($conditions);
    }

    $element['toolbar']['visibility']['add'] = [
      '#type' => 'details',
      '#title' => $this->t('Add condition'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#access' => !empty($conditions),
    ];

    $element['toolbar']['visibility']['add']['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Condition'),
      '#title_display' => 'invisible',
      '#options' => $conditions,
      '#empty_option' => '- ' . $this->t('Select') . ' -',
    ];

    $element['toolbar']['visibility']['add']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#validate' => ['::addToolbarVisibilityConditionValidate'],
      '#submit' => ['::addToolbarVisibilityConditionSubmit'],
      '#limit_validation_errors' => [
        ['toolbar', '_visibility', 'add'],
      ],
      '#ajax' => [
        'wrapper' => 'oidc-mcpf-toolbar-visibility',
        'callback' => '::toolbarVisibilityAjax',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the IDM roles.
    if ($form_state->getValue(['idm', 'roles_claim'])) {
      $roles_mapping = $form_state->getValue(['idm', 'roles_mapping'], []);

      foreach ($roles_mapping as $rid => $idm_role) {
        if ($idm_role === '') {
          $form_state->setErrorByName('idm][roles_mapping][' . $rid, $this->t('The mapped IDM role must be specified.'));
        }
        elseif (!preg_match('/^[A-Za-z]+(?::[a-z]+|-[a-z0-9]+:[A-Z0-9]+)?$/', $idm_role)) {
          $form_state->setErrorByName('idm][roles_mapping][' . $rid, $this->t('The IDM role is not in a valid format.'));
        }
      }
    }

    if (!$form_state->getValue(['toolbar', 'enabled'])) {
      return;
    }

    // Ensure a widget ID is specified.
    if (!$form_state->getValue(['toolbar', 'widget_id'])) {
      $form_state->setErrorByName('toolbar][widget_id', $this->t('@title field is required.', [
        '@title' => $this->t('Widget ID'),
      ]));
    }

    // Validate the visibility conditions.
    foreach ($form_state->get('toolbar_visibility') as $plugin_id => $configuration) {
      $condition_form = &$form['toolbar']['toolbar']['visibility']['conditions'][$plugin_id]['subform'];
      $condition_form_state = SubformState::createForSubform($condition_form, $form, $form_state);

      $condition = $this->getConditionPlugin($plugin_id);
      $condition->validateConfigurationForm($condition_form, $condition_form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Settings.
    $this->config('oidc_mcpf.settings')
      ->set('environment', $form_state->getValue(['settings', 'environment']))
      ->save();

    // IDM configuration.
    $roles_claim = $form_state->getValue(['idm', 'roles_claim']);
    $roles_mapping = [];

    if ($roles_claim) {
      $roles_mapping = $form_state->getValue(['idm', 'roles_mapping']);
    }

    $this->config('oidc_mcpf.idm')
      ->set('roles_claim', $roles_claim ?: NULL)
      ->set('roles_mapping', $roles_mapping ?: NULL)
      ->save();

    // Toolbar configuration.
    $config = $this->config('oidc_mcpf.toolbar');
    $enabled_old = $config->get('enabled');
    $enabled_new = (bool) $form_state->getValue(['toolbar', 'enabled']);
    $visibility = [];

    if ($enabled_new) {
      // Add the visibility conditions.
      foreach ($form_state->get('toolbar_visibility') as $plugin_id => $configuration) {
        $condition_form = &$form['toolbar']['toolbar']['visibility']['conditions'][$plugin_id]['subform'];
        $condition_form_state = SubformState::createForSubform($condition_form, $form, $form_state);

        $condition = $this->getConditionPlugin($plugin_id);
        $condition->submitConfigurationForm($condition_form, $condition_form_state);

        $configuration = $condition->getConfiguration();
        unset($configuration['id']);

        $visibility[$plugin_id] = $configuration;
      }
    }

    $config
      ->set('enabled', $enabled_new)
      ->set('widget_id', $form_state->getValue(['toolbar', 'widget_id']) ?: NULL)
      ->set('visibility', $visibility)
      ->save();

    if ($enabled_old || $config->get('enabled')) {
      $this->libraryDiscovery->clearCachedDefinitions();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Form validation handler; Validates the new IDM roles mapping entry.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addIdmRolesMappingEntryValidate(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue(['idm', '_roles_mapping', 'add', 'rid'])) {
      $form_state->setErrorByName('idm][_roles_mapping][add][rid', $this->t('Select the role to add.'));
    }
  }

  /**
   * Form submit handler; Add an entry to the IDM roles mapping.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addIdmRolesMappingEntrySubmit(array &$form, FormStateInterface $form_state) {
    $roles_mapping = &$form_state->get('idm_roles_mapping');
    $rid = $form_state->getValue(['idm', '_roles_mapping', 'add', 'rid']);

    $roles_mapping[$rid] = '';

    $user_input = $form_state->getUserInput();
    unset($user_input['idm']['_roles_mapping']['add']);
    $form_state->setUserInput($user_input);

    $form_state->setRebuild();
  }

  /**
   * Form submit handler; Delete an entry from the IDM roles mapping.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function deleteIdmRolesMappingEntrySubmit(array &$form, FormStateInterface $form_state) {
    $roles_mapping = &$form_state->get('idm_roles_mapping');
    $rid = $form_state->getTriggeringElement()['#rid'];

    unset($roles_mapping[$rid]);

    $form_state->setRebuild();
  }

  /**
   * Ajax callback; Returns the ajax response when the IDM roles mapping was changed.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Replacement content for the IDM roles mapping element.
   */
  public function idmRolesMappingAjax(array $form, FormStateInterface $form_state) {
    return $form['idm']['roles_mapping'];
  }

  /**
   * Form validation handler; Validates the new toolbar visibility condition.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addToolbarVisibilityConditionValidate(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue(['toolbar', '_visibility', 'add', 'plugin_id'])) {
      $form_state->setErrorByName('toolbar][_visibility][add][plugin_id', $this->t('Select the condition to add.'));
    }
  }

  /**
   * Form submit handler; Add a toolbar visibility condition.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addToolbarVisibilityConditionSubmit(array &$form, FormStateInterface $form_state) {
    $visibility = &$form_state->get('toolbar_visibility');
    $plugin_id = $form_state->getValue(['toolbar', '_visibility', 'add', 'plugin_id']);

    $visibility[$plugin_id] = NULL;

    $user_input = $form_state->getUserInput();
    unset($user_input['toolbar']['_visibility']['add']);
    $form_state->setUserInput($user_input);

    $form_state->setRebuild();
  }

  /**
   * Form submit handler; Delete a toolbar visibility condition.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function deleteToolbarVisibilityConditionSubmit(array &$form, FormStateInterface $form_state) {
    $visibility = &$form_state->get('toolbar_visibility');
    $plugin_id = $form_state->getTriggeringElement()['#plugin_id'];

    unset($visibility[$plugin_id]);

    $form_state->setRebuild();
  }

  /**
   * Ajax callback; Returns the ajax response when the IDM roles mapping was changed.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Replacement content for the IDM roles mapping element.
   */
  public function toolbarVisibilityAjax(array $form, FormStateInterface $form_state) {
    return $form['toolbar']['toolbar']['visibility'];
  }

  /**
   * Get a condition plugin instance.
   *
   * @param string $plugin_id
   *   The condition plugin ID.
   *
   * @return \Drupal\Core\Condition\ConditionInterface
   *   The condition plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getConditionPlugin($plugin_id) {
    if (!isset($this->conditionPlugins[$plugin_id])) {
      $this->conditionPlugins[$plugin_id] = $this->conditionManager->createInstance($plugin_id);
    }

    return $this->conditionPlugins[$plugin_id];
  }

}
