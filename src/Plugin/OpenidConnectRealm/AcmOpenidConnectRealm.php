<?php

namespace Drupal\oidc_mcpf\Plugin\OpenidConnectRealm;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\oidc\DisplayNameFormatOptionsTrait;
use Drupal\oidc\JsonHttp\JsonHttpClientInterface;
use Drupal\oidc\JsonHttp\JsonHttpPostRequestInterface;
use Drupal\oidc\OpenidConnectLoginException;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmBase;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmConfigurableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the ACM OpenID Connect realm plugin.
 *
 * @OpenidConnectRealm(
 *   id = "acm",
 *   name = @Translation("ACM"),
 * )
 */
class AcmOpenidConnectRealm extends OpenidConnectRealmBase implements OpenidConnectRealmConfigurableInterface {

  use DisplayNameFormatOptionsTrait;

  /**
   * Audiences.
   */
  public const AUDIENCE_CITIZEN = 'BUR';
  public const AUDIENCE_ORGANIZATION = 'EA';
  public const AUDIENCE_ASSOCIATION = 'VER';
  public const AUDIENCE_EDUCATION = 'OV';
  public const AUDIENCE_GOV_FLEMISH = 'GID';
  public const AUDIENCE_GOV_LOCAL = 'LB';

  /**
   * List of scopes that are automatically added.
   */
  protected const FIXED_SCOPES = ['openid', 'profile', 'phone', 'vo'];

  /**
   * The environment.
   *
   * @var string
   */
  protected $environment;

  /**
   * Class constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key-value storage factory.
   * @param \Drupal\oidc\JsonHttp\JsonHttpClientInterface $json_http_client
   *   The JSON HTTP client.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger channel.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, KeyValueFactoryInterface $key_value_factory, JsonHttpClientInterface $json_http_client, TimeInterface $time, LoggerChannelInterface $logger_channel) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $key_value_factory, $json_http_client, $time, $logger_channel);

    $this->setConfiguration($configuration);
    $this->environment = $config_factory->get('oidc_mcpf.settings')->get('environment');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('keyvalue'),
      $container->get('oidc.json_http_client'),
      $container->get('datetime.time'),
      $container->get('logger.channel.oidc')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = array_merge(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'enabled' => FALSE,
      'client_id' => NULL,
      'client_secret' => NULL,
      'audiences' => ['BUR'],
      'scopes' => [],
      'display_name_format' => '[user:name]',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => (int) $this->configuration['enabled'],
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#parents' => ['realms', 'acm', 'settings'],
      '#states' => [
        'visible' => [
          ':checkbox[name="realms[acm][settings][enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['wrapper']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $this->configuration['client_id'],
      '#states' => [
        'required' => [
          ':checkbox[name="realms[acm][settings][enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['wrapper']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#default_value' => $this->configuration['client_secret'],
      '#states' => [
        'required' => [
          ':checkbox[name="realms[acm][settings][enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['wrapper']['audiences'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Audiences'),
      '#description' => $this->t('The chosen audiences must be enabled on ACM as well.'),
      '#options' => [
        self::AUDIENCE_CITIZEN => $this->t('Citizens'),
        self::AUDIENCE_ORGANIZATION => $this->t('Companies and associations'),
        self::AUDIENCE_ASSOCIATION => $this->t('Factual associations'),
        self::AUDIENCE_EDUCATION => $this->t('Educational and training institutions'),
        self::AUDIENCE_GOV_FLEMISH => $this->t('Flemish government'),
        self::AUDIENCE_GOV_LOCAL => $this->t('Local governments'),
      ],
      '#default_value' => $this->configuration['audiences'],
      '#required' => TRUE,
    ];

    $form['wrapper']['scopes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional scopes'),
      '#description' => $this->t('Following scopes will be added automatically: @scopes', [
        '@scopes' => _oidc_mcpf_format_list(self::FIXED_SCOPES, TRUE),
      ]),
      '#default_value' => implode(' ', $this->configuration['scopes']),
    ];

    $form['wrapper']['display_name_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Display name format'),
      '#options' => $this->buildDisplayNameFormatOptions([
        '[user:account-name]',
        '[user:given-name]',
        '[user:given-name] [user:family-name]',
        '[user:given-name] [user:family-name-abbr]',
        '[user:mail]',
        '[user:organization-name]',
        '[user:organization-code]',
        '[user:organization-name] ([user:organization-code])',
        '[user:given-name] ([user:organization-name])',
        '[user:given-name] [user:family-name] ([user:organization-name])',
        '[user:given-name] [user:family-name-abbr] ([user:organization-name])',
        '[user:given-name] ([user:organization-code])',
        '[user:given-name] [user:family-name] ([user:organization-code])',
        '[user:given-name] [user:family-name-abbr] ([user:organization-code])',
      ]),
      '#default_value' => $this->configuration['display_name_format'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('enabled')) {
      return;
    }

    // Ensure a client ID is specified.
    if (!$form_state->getValue('client_id')) {
      $form_state->setErrorByName('client_id', $this->t('@title field is required.', [
        '@title' => $this->t('Client ID'),
      ]));
    }

    // Ensure a client secret is specified.
    if (!$form_state->getValue('client_secret')) {
      $form_state->setErrorByName('client_secret', $this->t('@title field is required.', [
        '@title' => $this->t('Client secret'),
      ]));
    }

    // Check if the audiences and display name format are compatible.
    $display_name_format = $form_state->getValue('display_name_format');

    if (strpos($display_name_format, '[user:organization-') === 0) {
      $audiences = $form_state->getValue('audiences');
      $audiences = array_values(array_diff($audiences, ['0']));

      if (in_array(self::AUDIENCE_CITIZEN, $audiences, TRUE)) {
        if (count($audiences) === 1) {
          $form_state->setErrorByName('display_name_format', $this->t('The display name format cannot contain organization details if they are not allow to log in.'));
        }
        else {
          $display_name_format = preg_replace('/\[user:organization-[a-z]+\]/', '', $display_name_format);

          if (strpos($display_name_format, '[user:') === FALSE) {
            $form_state->setErrorByName('display_name_format', $this->t('The display name format may not contain only organization details if citizens are allowed to log in.'));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($scopes = $form_state->getValue('scopes')) {
      $scopes = preg_split('/\s+/', str_replace(',', ' ', $scopes));
      $scopes = array_diff($scopes, self::FIXED_SCOPES);
      $scopes = array_unique($scopes);
    }

    $audiences = $form_state->getValue('audiences');
    $audiences = array_values(array_diff($audiences, ['0']));

    $this->configuration['enabled'] = (bool) $form_state->getValue('enabled');
    $this->configuration['client_id'] = $form_state->getValue('client_id');
    $this->configuration['client_secret'] = $form_state->getValue('client_secret');
    $this->configuration['scopes'] = $scopes ?: [];
    $this->configuration['audiences'] = $audiences;
    $this->configuration['display_name_format'] = $form_state->getValue('display_name_format');
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return $this->configuration['enabled'];
  }

  /**
   * {@inheritdoc}
   */
  public function getJsonWebTokensForLogin($state, $code) {
    $tokens = parent::getJsonWebTokensForLogin($state, $code);

    // Check the audience.
    $audience = $tokens->getClaim('vo_doelgroepcode') ?? self::AUDIENCE_CITIZEN;

    if (!in_array($audience, $this->configuration['audiences'], TRUE)) {
      if ($audience === self::AUDIENCE_CITIZEN) {
        $message = $this->t('You are not allowed to login as a citizen, please select an organization to represent.');
      }
      else {
        $message = $this->t('You are not allowed to login as a representative of the chosen organization.');
      }

      throw new OpenidConnectLoginException($message);
    }

    return $tokens;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayNameFormat() {
    return $this->configuration['display_name_format'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getClientId() {
    return $this->configuration['client_id'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getClientSecret() {
    return $this->configuration['client_secret'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getScopes() {
    $scopes = $this->configuration['scopes'];
    $scopes[] = 'profile';

    if (array_diff($this->configuration['audiences'], [self::AUDIENCE_CITIZEN])) {
      $scopes[] = 'phone';
      $scopes[] = 'vo';
    }

    return $scopes;
  }

  /**
   * {@inheritdoc}
   */
  protected function getIssuer() {
    $host = 'authenticatie.vlaanderen.be';

    if ($this->environment === 'test') {
      $host = 'authenticatie-ti.vlaanderen.be';
    }

    return 'https://' . $host . '/op';
  }

  /**
   * {@inheritdoc}
   */
  protected function getAuthorizationEndpoint() {
    return $this->getIssuer() . '/v1/auth';
  }

  /**
   * {@inheritdoc}
   */
  protected function getTokenEndpoint() {
    return $this->getIssuer() . '/v1/token';
  }

  /**
   * {@inheritdoc}
   */
  protected function getUserinfoEndpoint() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEndSessionEndpoint() {
    return $this->getIssuer() . '/v1/logout';
  }

  /**
   * {@inheritdoc}
   */
  protected function getJwksUrl() {
    return $this->getIssuer() . '/v1/keys';
  }

  /**
   * {@inheritdoc}
   */
  protected function getJsonWebTokens(JsonHttpPostRequestInterface $json_http_post_request) {
    $tokens = parent::getJsonWebTokens($json_http_post_request);
    $audience = $tokens->getClaim('vo_doelgroepcode') ?? self::AUDIENCE_CITIZEN;

    if ($audience !== self::AUDIENCE_CITIZEN) {
      // Build the audience-based ID.
      $audience_id = $audience . '_' . $tokens->getClaim('vo_orgcode');
      $audience_id .= '_' . $tokens->getId();

      // Update the claims.
      $tokens->setClaim('audience_id', $audience_id);
      $tokens->setIdClaim('audience_id');
      $tokens->setEmailClaim('vo_email');
    }

    return $tokens;
  }

}
