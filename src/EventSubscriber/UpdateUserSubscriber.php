<?php

namespace Drupal\oidc_mcpf\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\oidc\JsonWebTokens;
use Drupal\oidc\OpenidConnectSessionInterface;
use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthLoginEvent;
use Drupal\oidc_mcpf\Plugin\OpenidConnectRealm\AcmOpenidConnectRealm;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to update the synced user fields and roles on login.
 */
class UpdateUserSubscriber implements EventSubscriberInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * The IDM configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $idmConfig;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(OpenidConnectSessionInterface $session, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->session = $session;
    $this->idmConfig = $config_factory->get('oidc_mcpf.idm');
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ExternalAuthEvents::LOGIN][] = 'onLogin';

    return $events;
  }

  /**
   * Updates the synced user fields and roles on login.
   *
   * @param \Drupal\externalauth\Event\ExternalAuthLoginEvent $event
   *   The login event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onLogin(ExternalAuthLoginEvent $event) {
    if ($event->getProvider() !== 'oidc:acm') {
      return;
    }

    $tokens = $this->session->getJsonWebTokens();
    $audience = $tokens->getClaim('vo_doelgroepcode');

    // Not applicable for citizens.
    if (!$audience || $audience === AcmOpenidConnectRealm::AUDIENCE_CITIZEN) {
      return;
    }

    $account = $event->getAccount();

    // Update the fields.
    $save = $this->syncUserFields($account, $tokens);

    // Update the roles.
    $claim = $this->idmConfig->get('roles_claim');

    if ($claim) {
      $idm_roles = $tokens->getClaim($claim) ?: [];

      if (is_string($idm_roles)) {
        $idm_roles = explode(',', $idm_roles);
      }

      $save |= $this->syncUserRoles($account, $idm_roles);
    }

    // Save if there are changes.
    if ($save) {
      $account->save();
    }
  }

  /**
   * Synchronize our user fields with their matching claims.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user that just logged in.
   * @param \Drupal\oidc\JsonWebTokens $tokens
   *   The received JSON web tokens.
   *
   * @return bool
   *   Boolean indicating if the account was updated.
   */
  protected function syncUserFields(UserInterface $account, JsonWebTokens $tokens) {
    $updated = FALSE;

    // Update the phone number.
    $phone = $tokens->getClaim('phone_number');

    if ($phone !== NULL && $phone !== $account->get('phone')->value) {
      $account->set('phone', $phone);
      $updated = TRUE;
    }

    // Update the organization name.
    $organization_name = $tokens->getClaim('vo_orgnaam');

    if ($organization_name !== NULL && $organization_name !== $account->get('organization_name')->value) {
      $account->set('organization_name', $organization_name);
      $updated = TRUE;
    }

    // Update the organization code.
    $organization_code = $tokens->getClaim('vo_orgcode');

    if ($organization_code !== NULL && $organization_code !== $account->get('organization_code')->value) {
      $account->set('organization_code', $organization_code);
      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Synchronize the Drupal roles with their matching IDM roles.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user that just logged in.
   * @param array $idm_roles
   *   The retrieved IDM roles.
   *
   * @return bool
   *   Boolean indicating if roles were changed.
   */
  protected function syncUserRoles(UserInterface $account, array $idm_roles) {
    $updated = FALSE;

    // Get the mapping.
    if (!$mapping = $this->idmConfig->get('roles_mapping')) {
      return $updated;
    }

    // Get the existing and mapped account roles.
    $account_roles = $account->getRoles(TRUE);
    $account_roles = array_intersect($account_roles, array_keys($mapping));

    if (!$idm_roles) {
      // Remove all mapped roles.
      $remove_rids = $account_roles;
      $add_rids = [];
    }
    else {
      // Check if the IDM roles are in the <role>-<context>:<scope> format.
      // If so, duplicate them in the <role>:<context> format.
      if (strpos(reset($idm_roles), '-') !== FALSE) {
        $tmp = $idm_roles;
        foreach ($tmp as $idm_role) {
          $idm_roles[] = str_replace('-', ':', strstr($idm_role, ':', TRUE));
        }
      }

      // Collect the added and removed roles.
      if ($add_rids = array_intersect($mapping, $idm_roles)) {
        $add_rids = array_keys($add_rids);
      }

      $remove_rids = [];
      if ($account_roles) {
        $remove_rids = $account_roles;

        if ($add_rids) {
          $remove_rids = array_diff($remove_rids, $add_rids);
          $add_rids = array_diff($add_rids, $account_roles);
        }
      }
    }

    // Apply the changes.
    foreach ($add_rids as $rid) {
      $account->addRole($rid);
      $updated = TRUE;
    }

    foreach ($remove_rids as $rid) {
      $account->removeRole($rid);
      $updated = TRUE;
    }

    return $updated;
  }

}
