Changelog
=========

All notable changes to the OIDC My Citizen Profile Flanders module.

## 1.0.3 (2024-04-16):

### Added

* Factual associations audience (#3441107)


## 1.0.2 (2023-06-07):

### Added

* Drupal 10 compatibility


## 1.0.1 (2022-10-21):

### Fixed

* Widget API URLs


## 1.0.0 (2022-05-31):

### Changed

* Require OpenID Connect Client 2.x
* Renamed `oidc_mcpf.general` config to `oidc_mcpf.settings`
* Refactored toolbar library

### Fixed

* Code style errors

### Removed

* Drupal 8 compatibility


## 1.0.0-alpha3 (2021-06-03):

### Added

* Support for visibility conditions to manage the toolbar visibility

### Fixed

* Roles order in config form


## 1.0.0-alpha2 (2020-10-20):

### Added

* Display name formats with organization details
* Implemented the display name format options helper trait

### Fixed

* Deprecated `[user:name]` token replaced by `[user:account-name]`


## 1.0.0-alpha1 (2020-09-22):

First alpha release.
