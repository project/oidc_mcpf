OIDC My Citizen Profile Flanders
================================

Provided integration with ACM/IDM, also known as the My Citizen Profile of the Flemish Government.


## Requirements

* [OpenID Connect Client](https://www.drupal.org/project/oidc)


## Installation

First of all download the latest release and its dependencies using `composer require drupal/oidc_mcpf`.
You can also download it manually, but please make sure all requirements are fulfilled before continuing.

Next, simply enable the module as described in the [the module installation guide](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

### General

You can configure the environment, IDM roles mapping and toolbar at `/admin/config/people/oidc/mcpf`.


#### Environment

You can specify whether you are using the test or production environment.


#### IDM roles

The module allows you to map any IDM roles to Drupal roles.

To use this functionality you must first configure the required roles scope (review your
onboarding documents for more details) in the realm settings.

Next specify which claim contains the roles. When specified the mapping table should appear,
here you can add Drupal roles and map them to an IDM role.

The IDM roles should be specified in one of these formats, according to your onboarding documents:
* `<role>-<context>:<scope>`
* `<role>:<context>`
* `<role>`


#### Toolbar

You can enable the My Citizen Profile toolbar. Please note that you must have a widget ID to do so.


### ACM realm

You can enable and configure the ACM realm at `/admin/config/people/oidc/realms`.
Simply fill out the configuration form and save the changes.
